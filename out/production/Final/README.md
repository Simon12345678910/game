# Rock Paper Scissors (CPU)

This page describes the functionality of our team project, especially my part of the work which is a self programmed version of Rock Paper Scissors.
## General Informations
My whole part of this project is to make a working Rock Paper Scissors game with Java. The game consists of two files. "RPS.java" (the actual game where all the code is) and "RPS.form" where the user inferface was build. 

## Methods and global Variables
To make this game working, we will need some global variables which are used to detect the winner.
```
//setup for the game and the used variables  
public String[] rps = {"r", "p", "s"}; //usage for comparison  
public String cpu_move; //will be set depending on which button is clicked  
public String player_move; //same like cpu_move  
public String winner; //will be set depending on the winner  
  
int count = 0; //to count the amount of games
```
There is only one method named "GetWinner". It takes in the CPU and USER as a String and checks when somebody has won and with which move the game ended. Depending on the input, the result will vary.

````
//method to find out who the winner is  
public static String getWinner(String cpu, String player) {  
  
    //rock against paper --> paper wins  
  if (cpu.equals("r") && player.equals("p")) {  
        return "Paper is stronger than Rock! Player Won!";  
  } else if (cpu.equals("p") && player.equals("r")) {  
        return "Paper is stronger than Rock! Cpu Won!";  
  }  
  
    //rock against scissors --> rock wins  
  if (cpu.equals("r") && player.equals("s")) {  
        return "Rock demolishes Scissors! CPU Won!";  
  } else if (cpu.equals("s") && player.equals("r")) {  
        return "Rock demolishes Scissors! Player Won!";  
  }  
  
    //paper against scissors --> scissors wins  
  if (cpu.equals("s") && player.equals("p")) {  
        return "Scissors  cuts Paper! CPU Won!";  
  } else if (cpu.equals("p") && player.equals("s")) {  
        return "Scissors  cuts Paper! Player Won!";  
  }  
  
    //if player_move equals cpu_move  
  return "It is a tie! You both are strong!";  
}
````

## Buttons
In total, three buttons are needed to detect and store the CPU and USER input into a variable. All buttons have the same code, only the output message box varies. The button for "Rock" is shown in this example.
````
rockButton.addActionListener(e -> {  
  
  count++;  
  
  player_move = "r";  
  cpu_move = rps[new Random().nextInt(rps.length)];  
  
  txtPlayer.setText("Rock");  
  txtCPU.setText(convertCPULetter(cpu_move));  
  txtCount.setText(count + "");  
  
  winner = getWinner(cpu_move, player_move);  
  JOptionPane.showMessageDialog(frame,  
  winner);  
  
}
````
## JCombo Box
A JCombo Box is also used to give this little program more feautures, in this case "New Game", "Help" and "Quit".
````
comboBox.addActionListener(e -> {  
  
    String val = Objects.requireNonNull(comboBox.getSelectedItem()).toString();  
    
 if (val.equals("New Game")) {  
 
  txtCPU.setText("");  
  txtPlayer.setText("");  
  txtCount.setText("");  
  
  }  
    if (val.equals("Help")) {  
        JOptionPane.showMessageDialog(frame,  
  "This is just the classical rock paper scissors game," +  
                        " you dont need any help!");  
  }  
    if (val.equals("Quit")) System.exit(1);  
}
````


[RPS.form](uploads/d61cc4368acb5c1ab2d2b3177cb2ce97/RPS.form)

[RPS.java](uploads/9d1a12a4aeeefa266e9e200c47964639/RPS.java)
