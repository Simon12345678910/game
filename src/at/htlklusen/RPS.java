package at.htlklusen;

import javax.swing.*;
import java.util.Objects;
import java.util.Random;

public class RPS {

    public JPanel mainPanel;
    private JButton rockButton;
    private JButton paperButton;
    private JButton scissorsButton;
    private JTextField txtPlayer;
    private JTextField txtCPU;
    private JTextField txtCount;
    private JComboBox comboBox;
    static JFrame frame;

    //setup for the game and the used variables
    public String[] rps = {"r", "p", "s"};
    public String cpu_move;
    public String player_move;
    public String winner;

    int count = 0;

    //method to find out who the winner is
    public static String getWinner(String cpu, String player) {

        //rock against paper --> paper wins
        if (cpu.equals("r") && player.equals("p")) {
            return "Paper is stronger than Rock! Player Won!";
        } else if (cpu.equals("p") && player.equals("r")) {
            return "Paper is stronger than Rock! Cpu Won!";
        }

        //rock against scissors --> rock wins
        if (cpu.equals("r") && player.equals("s")) {
            return "Rock demolishes Scissors! CPU Won!";
        } else if (cpu.equals("s") && player.equals("r")) {
            return "Rock demolishes Scissors! Player Won!";
        }

        //paper against scissors --> scissors wins
        if (cpu.equals("s") && player.equals("p")) {
            return "Scissors  cuts Paper! CPU Won!";
        } else if (cpu.equals("p") && player.equals("s")) {
            return "Scissors  cuts Paper! Player Won!";
        }

        //if player_move equals cpu_move
        return "It is a tie! You both are strong!";
    }

    public static String convertCPULetter(String cpu) {

        if (cpu.equals("r")) return "Rock";
        if (cpu.equals("p")) return "Paper";
        if (cpu.equals("s")) return "Scissors";

        return "";

    }

    //rock button
    public RPS() {
        rockButton.addActionListener(e -> {

            count++;

            player_move = "r";
            cpu_move = rps[new Random().nextInt(rps.length)];

            txtPlayer.setText("Rock");
            txtCPU.setText(convertCPULetter(cpu_move));
            txtCount.setText(count + "");

            winner = getWinner(cpu_move, player_move);
            JOptionPane.showMessageDialog(frame,
                    winner);

        });

        //paper button
        paperButton.addActionListener(e -> {

            count++;

            player_move = "p";
            cpu_move = rps[new Random().nextInt(rps.length)];

            txtPlayer.setText("Paper");
            txtCPU.setText(convertCPULetter(cpu_move));
            txtCount.setText(count + "");

            winner = getWinner(cpu_move, player_move);
            JOptionPane.showMessageDialog(frame,
                    winner);

        });
        //scissors button
        scissorsButton.addActionListener(e -> {

            count++;

            player_move = "s";
            cpu_move = rps[new Random().nextInt(rps.length)];

            txtPlayer.setText("Scissors");
            txtCPU.setText(convertCPULetter(cpu_move));
            txtCount.setText(count + "");

            winner = getWinner(cpu_move, player_move);
            JOptionPane.showMessageDialog(frame,
                    winner);

        });
        //combo box
        comboBox.addActionListener(e -> {

            String val = Objects.requireNonNull(comboBox.getSelectedItem()).toString();

            if (val.equals("New Game")) {

                txtCPU.setText("");
                txtPlayer.setText("");
                txtCount.setText("");

            }

            if (val.equals("Help")) {

                JOptionPane.showMessageDialog(frame,
                        "This is just the classical rock paper scissors game," +
                                " you dont need any help!");

            }

            if (val.equals("Quit")) System.exit(1);


        });
    }

    public static void main(String[] args) {
        JFrame frame = new JFrame("RPS");
        frame.setContentPane(new RPS().mainPanel);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
    }
}
