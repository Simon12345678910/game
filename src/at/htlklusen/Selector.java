package at.htlklusen;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class Selector {


    private JPanel selectorPanel;
    private JButton btnTic;
    private JButton btnCalc;


    public Selector() {
        btnTic.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (e.getSource() == btnTic) {

                    JFrame frame = new JFrame("Helloform");
                    frame.setContentPane(new Tictactoe().contentPanel);
                    //frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
                    frame.pack();
                    frame.setVisible(true);

                }
            }
        });
        btnCalc.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                if (e.getSource() == btnCalc) {

                    JFrame frame = new JFrame("RPS");
                    frame.setContentPane(new RPS().mainPanel);
                    frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
                    frame.pack();
                    frame.setVisible(true);


                }

            }
        });
    }

    public static void main(String[] args) {
        JFrame frame = new JFrame("Selector");
        frame.setContentPane(new Selector().selectorPanel);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);



    }
}
