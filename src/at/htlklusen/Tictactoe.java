package at.htlklusen;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.Color;


public class Tictactoe {
    public JPanel contentPanel;
    public JButton button1;
    public JButton button2;
    public JButton button3;
    public JButton button4;
    public JButton button5;
    public JButton button6;
    public JButton button7;
    public JButton button8;
    public JButton button9;
    static JFrame frame;

    static int count = 2;

    int[] array = {0, 100, 200, 300, 400, 500, 600, 700, 800, 900};

    public Tictactoe() {
        button1.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                if (e.getSource() == button1) {

                    if (count % 2 == 0) {

                        button1.setBackground(Color.red);
                        count++;
                        array[1] = 1;
                    } else {

                        button1.setBackground(Color.blue);
                        count++;
                        array[1] = 2;

                    }

                    win(array);

                }

            }
        });
        button2.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                if (e.getSource() == button2) {

                    if (count % 2 == 0) {

                        button2.setBackground(Color.red);
                        count++;
                        array[2] = 1;
                    } else {

                        button2.setBackground(Color.blue);
                        count++;
                        array[2] = 2;

                    }

                    win(array);
                }


            }
        });
        button3.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (e.getSource() == button3) {

                    if (count % 2 == 0) {

                        button3.setBackground(Color.red);
                        count++;
                        array[3] = 1;
                    } else {

                        button3.setBackground(Color.blue);
                        count++;
                        array[3] = 2;

                    }
                    win(array);
                }

            }
        });
        button4.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (e.getSource() == button4) {

                    if (count % 2 == 0) {

                        button4.setBackground(Color.red);
                        count++;
                        array[4] = 1;
                    } else {

                        button4.setBackground(Color.blue);
                        count++;
                        array[4] = 2;

                    }
                    win(array);
                }

            }
        });
        button5.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (e.getSource() == button5) {

                    if (count % 2 == 0) {

                        button5.setBackground(Color.red);
                        count++;
                        array[5] = 1;
                    } else {

                        button5.setBackground(Color.blue);
                        count++;
                        array[5] = 2;

                    }
                    win(array);
                }

            }
        });
        button6.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (e.getSource() == button6) {

                    if (count % 2 == 0) {

                        button6.setBackground(Color.red);
                        count++;
                        array[6] = 1;
                    } else {

                        button6.setBackground(Color.blue);
                        count++;
                        array[6] = 2;

                    }
                    win(array);
                }

            }
        });
        button7.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (e.getSource() == button7) {

                    if (count % 2 == 0) {

                        button7.setBackground(Color.red);
                        count++;
                        array[7] = 1;
                    } else {

                        button7.setBackground(Color.blue);
                        count++;
                        array[7] = 2;

                    }
                    win(array);
                }

            }
        });
        button8.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (e.getSource() == button8) {

                    if (count % 2 == 0) {

                        button8.setBackground(Color.red);
                        count++;
                        array[8] = 1;
                    } else {

                        button8.setBackground(Color.blue);
                        count++;
                        array[8] = 2;
                    }
                    win(array);
                }

            }
        });
        button9.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (e.getSource() == button9) {

                    if (count % 2 == 0) {

                        button9.setBackground(Color.red);
                        count++;
                        array[9] = 1;
                    } else {

                        button9.setBackground(Color.blue);
                        count++;
                        array[9] = 2;
                    }
                    win(array);


                }

            }
        });
    }

    public static void main(String[] args) {
        frame = new JFrame("Helloform");
        frame.setContentPane(new Tictactoe().contentPanel);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
    }

    public boolean win(int[] arrayx) {

        boolean b = false;


        if (arrayx[1] == arrayx[2] && arrayx[2] == arrayx[3] || arrayx[4] == arrayx[5] && arrayx[5] == arrayx[6] || arrayx[7] == arrayx[8] && arrayx[8] == arrayx[9] || arrayx[1] == arrayx[4] && arrayx[4] == arrayx[7] || arrayx[2] == arrayx[5] && arrayx[5] == arrayx[8] || arrayx[3] == arrayx[6] && arrayx[6] == arrayx[9] || arrayx[1] == arrayx[5] && arrayx[5] == arrayx[9] || arrayx[3] == arrayx[5] && arrayx[5] == arrayx[7]) {

            //custom title, custom icon
            JOptionPane.showMessageDialog(frame,
                    "Sie haben gewonnen!!!",
                    "Inane custom dialog",
                    JOptionPane.PLAIN_MESSAGE);

            button1.setEnabled(false);
            button2.setEnabled(false);
            button3.setEnabled(false);
            button4.setEnabled(false);
            button5.setEnabled(false);
            button6.setEnabled(false);
            button7.setEnabled(false);
            button8.setEnabled(false);
            button9.setEnabled(false);


            b = true;



        }
        if (count == 11) {
            //custom title, custom icon
            JOptionPane.showMessageDialog(frame,
                    "Unentschieden!!!",
                    "Inane custom dialog",
                    JOptionPane.PLAIN_MESSAGE);


        }

        return b;
    }

}
